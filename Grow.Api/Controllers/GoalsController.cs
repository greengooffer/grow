﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Grow.Api.Controllers
{
    [RoutePrefix("[controller]")]
    public class GoalsController : ApiController
    {
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new[] {"goal1, goal2"};
        }
    }
}
